# Logiciel Reconnaissance plaque d'immatriculation

## Contexte
Lors de ma seconde année de BTS, j'ai participé à un projet de fin d'étude sur la gestion d'un camping divisé en 3 parties :
- **Gestion de la barrière d'entrée en fonction des plaques d'immatriculation des véhicules en temps réel.**
- Suivi en temps réel de la consommation d'eau et de courant de chaque emplacement de camping.
- Réalisation d'une IHM WEB permettant la visualisation de toutes les données.

Nous étions 3 élèves à travailler sur ce projet et chacun d'entre nous devais s'occuper de sa partie. J'ai donc travaillé sur le logiciel de reconnaissance de plaque.

Ce projet à durée 6 mois et chaque semaines j'avais 16 heures dédiées pour ce projet.
Je devais donc concevoir un logiciel capable de détecter, analyser, et décoder une plaque d'immatriculation de voiture en temps réel. Comme contrainte, le logiciel devait également être fonctionnel sur un ordinateur à système ARM et avec une relativement faible puissance de calcul.

> **J'ai mis à disposition le rapport écrit complet que j'ai envoyé au jury, à la racine de ce dépôt.** Le reste du readme est un condensé de ce rapport.

## Matériels utilisés 
- Caméra mvBlueFox
- Mini-Pc BeagleBone Black

## Librairies utilisées
- mvIMPACT SDK (librairie C++ pour utiliser la caméra mvBlueFox)
- OpenCV
- Tesseract

## Solution mis en place

- Utilisation d'un algorithme d'apprentissage, nommé Haartraining, que opencv met à disposition, pour détecter si une plaque est présente sur une image.
- Lorsqu'une plaque est détecté, le logiciel va isolé la plaque du reste de l'image.
- Ensuite on isole les caractères (on retire tout ce qui n'est pas un caractère sur la plaque)
- Utilisation de Tesseract pour lire les caractères ainsi isolés.

## Cross Compilation

La cross compilation est le sujet qui m'a donné le plus de mal. En effet comme pour tout le reste du projet, je n'avais aucune connaissance préalable sur le sujet et je pensais naïvement que ça ne serait pas très difficile la mettre en place.
Au final je suis parvenu à mettre en place mon système de cross compilation. J'ai pu développer l'application sur mon environnement linux habituel et choisir de compiler mon projet pour une architecture 64 bits ou ARM.

## Conclusion 

J'ai beaucoup apprécié réaliser ce projet. Car en plus de m'avoir permis de mettre en
application certaine connaissance acquises au cours de ces deux années de BTS, il
m'aura également permis d'apprendre de nombreuse choses dans différents domaines.
Avant ce projet je n'avais jamais travaillé sur de la reconnaissance optique de
caractère et sur de la reconnaissance d'objet sur une image. De même en ce qui
concerne le portage sur beaglebone black, je connaissais le principe de cross
compilation, mais je n'avais jamais eu à le faire moi même. J'ai également beaucoup apprécié
travailler sur la caméra, car le domaine de la vidéo m'a toujours intéressée et ce projet
m'a permis d'en apprendre d'avantage, de plus c'était la première fois que j'utilisais
une caméra industrielle.
