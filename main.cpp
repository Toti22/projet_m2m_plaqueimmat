#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

/* Pour fprintf() */
#include <stdio.h>
/* Pour fork() */
#include <unistd.h>
/* Pour perror() et errno */
#include <errno.h>
/* Pour le type pid_t */
#include <sys/types.h>
/* Pour wait() */
#include <sys/wait.h>

// pour gestion fichier

#include <iostream>

#include <fstream>
#include <stdio.h>
#include <math.h>
#include <unistd.h>

#include <tesseract/baseapi.h>
#include <tesseract/strngs.h>
#include <leptonica/allheaders.h>
#include <tesseract/ocrclass.h>

#define RATIO 0.2


using namespace std;
using namespace cv;

pid_t creationProcessus(void)
{
    /* On crée une nouvelle valeur de type pid_t */
    pid_t pid;

    /* On fork() tant que l'erreur est EAGAIN */
    //do {
    pid = fork();
    //} while ((pid == -1) && (errno == EAGAIN));

    /* On retourne le PID du processus ainsi créé */
    return pid;
}

void processusFils(string nomProgrammeCamera,char *arg[])
{
    if (execv(nomProgrammeCamera.c_str(), arg) == -1)
    {
        perror("execv");
        return ;
    }
}

void findMinMax(std::vector<Point> contours,int *x_minimum,int *y_minimum, int *x_maximum,int *y_maximum)
{
    *x_minimum=contours[0].x;
    *x_maximum=contours[0].x;
    *y_minimum=contours[0].y;
    *y_maximum=contours[0].y;
    for (int i=0;i<contours.size();i++)
    {
        if (*x_minimum>contours[i].x)*x_minimum=contours[i].x;
        if (*x_maximum<contours[i].x)*x_maximum=contours[i].x;
        if (*y_minimum>contours[i].y)*y_minimum=contours[i].y;
        if (*y_maximum<contours[i].y)*y_maximum=contours[i].y;
    }
}

void findHaartraining(Mat image, vector<Rect>& roi,double ratioObjet,int nombreObjetPossible=1)
{
    CascadeClassifier plateCascade;
    plateCascade.load( "./cascade5.xml" );
    vector<Rect> rectTemp;
    //for (double i =ratioObjet/10;i<ratioObjet;i+=ratioObjet/10)
    {
        //cout << "min    x: " << image.cols*i << "  y: " << image.cols*RATIO*i << "  max x: " << image.cols*(i+0.3) << "     y: " << image.cols*RATIO*(i+0.3) << endl;
        //plateCascade.detectMultiScale( image, rectTemp, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, Size(image.cols*i, image.cols*ratioObjet*i),Size(image.cols*(i+ratioObjet/8), image.cols*RATIO*(i+ratioObjet/8) ));
        plateCascade.detectMultiScale( image, rectTemp, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, Size(800, 160),Size(1000, 350));
        if (rectTemp.size()<=nombreObjetPossible)
        {
            for (int y=0;y<rectTemp.size();roi.push_back(rectTemp[y]),y++);

        }

        rectTemp.clear();
    }
}

bool findPlate(Mat image,Rect& plate,vector<Rect>*listRoi=0, int compt=0)
{
    //cout << "||||| !!! FINDPLATE !!! |||||" << endl << endl;
    //if (listRoi==0)cout << " sans haartraining ";
    //else if (listRoi->size()==0)cout << " sans haartraining (size==0) ";
    //else cout << "avec haartraining" <<  endl;
    Mat imageTemp;
    Mat imageOriginal = image.clone();
    if (listRoi!=0)
    {
        if (listRoi->size()>0)
        {
            // << "AVANT : x :" <<  (*listRoi)[0].x << " y : " << (*listRoi)[0].y << " width : " << (*listRoi)[0].width << " height : "  << (*listRoi)[0].height << endl;         // J'aggrandi de moitié la region d'intérêt trouvé par le haartraining pour etre sur d'avoir toute la plaque

            (*listRoi)[0].y=int((*listRoi)[0].y-int(int(((*listRoi)[0].height)/2)));
            if ((*listRoi)[0].y<0)(*listRoi)[0].y=0;
            (*listRoi)[0].width=(*listRoi)[0].width*1.2;
            if ((*listRoi)[0].width > image.cols)(*listRoi)[0].width=image.cols-(*listRoi)[0].x;
            (*listRoi)[0].height=(*listRoi)[0].height*1.2;
            if ((*listRoi)[0].height > image.rows)(*listRoi)[0].height=image.rows-(*listRoi)[0].y;

            // on garde que la région d'intérêt trouvé.
            image = image((*listRoi)[0]);
            listRoi->erase(listRoi->begin());
            string baba="plaqueHaartraining";
            baba+='0'+compt;
            baba+=".jpg";
            //cout << "SAUVEGARDE PLAQUE HAARTRAINING !!!!!" << endl;
            imwrite(baba,image);
        }
    }

    vector<vector<Point> > contours;
    vector<vector<Point> > contours2;
    vector <Rect>listChar;
    int x_min,x_max,y_min,y_max;
    findContours(image,contours,CV_RETR_LIST,CV_CHAIN_APPROX_NONE);

    for (int y=0;y<contours.size();y++)
    {
        findMinMax(contours[y],&x_min,&y_min,&x_max,&y_max);
        rectangle( image,Point(x_min,y_min),Point(x_max,y_max) , Scalar( 255, 0, 255 ),1);
        if (double(y_max-y_min)/double(x_max-x_min)<=0.19 || double(y_max-y_min)/double(x_max-x_min)>=0.30)continue;
        if (x_max-x_min<=0 || y_max-y_min<=0)continue;

        CvRect ROI = cvRect(x_min,y_min,x_max-x_min,y_max-y_min);
        imageTemp = image(ROI);

        findContours(imageTemp,contours2,CV_RETR_LIST,CV_CHAIN_APPROX_NONE);

        int compteurCaractere=0;
        compteurCaractere=0;
        for (int i=0;i<contours2.size();i++)
        {

            findMinMax(contours2[i],&x_min,&y_min,&x_max,&y_max);


            if (y_max-y_min>=double(imageTemp.rows)/2 && y_max-y_min<= double(imageTemp.rows)/1.06 )
            {
                listChar.push_back(Rect(x_min, y_min,x_max-x_min,y_max-y_min));

                //cout << "numéro forme :" << i << " un caractère trouvé" << endl;
                compteurCaractere++;

            }
        }

        if (compteurCaractere>=5 && compteurCaractere<=10){
                plate = ROI;
                return true;
        }
        else{
            listChar.clear();
            compteurCaractere=0;
        }

        contours2.clear();
    }
    if (listRoi!=0)
    {
        if (listRoi->size()>0)findPlate(imageOriginal,plate,listRoi);
        else return false;
    }
    else return false;
}

int isolerCaractere(Mat& image, int& aireDesContours)
{
    image = image(Rect(0,0,int(image.cols),int(image.rows)));

    int nbCaractere=0;
    int x_minDernierCaractere=0,x_maxDernierCaractere=0;
    int ecartXMoyen=0,posYMoyen=0,largeurMoyenne=0,hauteurMoyenne=0;
    vector<vector<Point> > contours;
    findContours(image.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
    int x_min,x_max,y_min,y_max;
    for (int i = 0; i < contours.size(); i++)
    {

        findMinMax(contours[i],&x_min,&y_min,&x_max,&y_max);
        if (
        y_max-y_min<=double(image.rows)*0.5 || y_max-y_min>= double(image.rows)*0.95 ||
        x_max-x_min<=double(image.cols)*0.04 || x_max-x_min>=double(image.cols)*0.15)drawContours(image, contours, i, Scalar(0), -1);
        else
        {
            Mat imageTemp = image(Rect(x_min,y_min,x_max-x_min,y_max-y_min));
            vector<vector<Point> > contoursTemp;
            findContours(imageTemp.clone(), contoursTemp, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
            if (contoursTemp.size()>3)
            {
                drawContours(image, contours, i, Scalar(0), -1);
            }
            else
            {
                x_minDernierCaractere=x_min;
                x_maxDernierCaractere=x_max;
                if (nbCaractere!=0)
                {
                    ecartXMoyen+=x_min-(x_maxDernierCaractere-x_min);
                }
                largeurMoyenne+=x_max-x_min;
                hauteurMoyenne+=y_max-y_min;
                posYMoyen+=y_min;
                nbCaractere++;
            }
        }
    }
    contours.clear();
    if (nbCaractere>=1)
    {
        posYMoyen/=nbCaractere;
        largeurMoyenne/=nbCaractere;
        hauteurMoyenne/=nbCaractere;
    }
    if (nbCaractere>1)ecartXMoyen/=nbCaractere-1;

    int coefDeChauqeCaractere = 1/nbCaractere;


    findContours(image.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
    for (int i = 0; i < contours.size(); i++)
    {
        findMinMax(contours[i],&x_min,&y_min,&x_max,&y_max);
        if ((y_max-y_min)<=hauteurMoyenne*0.8 || (y_max-y_min)>=hauteurMoyenne*1.2 &&
            y_min<=posYMoyen*0.8 || y_min>=posYMoyen*1.2 )
        {
            drawContours(image, contours, i, Scalar(0), -1);
            nbCaractere--;
        }
    }

    contours.clear();

    findContours(image.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
    for (int i = 0; i < contours.size(); i++)
    {
        if (contourArea(contours[i]) < 100)drawContours(image, contours, i, Scalar(0), -1);
    }
    rectangle( image, Point(0,0),Point(image.cols,image.rows) , Scalar( 0 ),3);

    contours.clear();

    findContours(image.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
    for (int i = 0; i < contours.size(); i++)
    {
        aireDesContours+=contourArea(contours[i]);
    }

    findContours(image.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
    for (int i = 0; i < contours.size(); i++)
    {
        if (contourArea(contours[i]) < 100)drawContours(image, contours, i, Scalar(0), -1);
    }

    return nbCaractere;
}

void ocr(Mat& image)
{
    imshow("image avant ocr",image);
    tesseract::TessBaseAPI tess;
    tess.Init(NULL, "eng", tesseract::OEM_DEFAULT);

    // on l'informe de la liste des caractère possible à décoder
    tess.SetVariable("tessedit_char_whitelist", "0123456789AZERTYUIOPQSDFGHJKLMWXCVBN");
    tess.SetPageSegMode(tesseract::PSM_SINGLE_BLOCK);
    tess.SetImage((uchar*)image.data, image.cols, image.rows, 1, image.cols);
    char *out =tess.GetUTF8Text();
    std::cout << std::endl << "plaque d\'immatriculation : " << out << std::endl;
}

int main(int argc, char*argv[] )
{
    string nomProgrammeCamera="./camera";
    string argumentProgramme;
    string nomImage;
    bool modeImage=false;
    for (int i=1;i<argc;i++)
    {
        bool option=false;;
        string stringOption;
        string argument="";
        for (int j=0;argv[i][j]!='\0';j++)
        {
            if (argv[i][j]=='=' && option==false){option=true;continue;}
            if (option==true)stringOption+=argv[i][j];
            else argument+=argv[i][j];
        }

        if (argument=="MODE")
        {
            if (stringOption=="IMAGE")modeImage=true;
        }
        else if (argument=="ARGUMENT")
        {
            argumentProgramme=stringOption;
        }
        else if (argument=="NOM_PROGRAMME")
        {
            nomProgrammeCamera=stringOption;
        }
        else if (argument=="NOM_IMAGE")
        {
            nomImage=stringOption;
        }
        stringOption.clear();
    }

    Mat image;
    Mat imagePlaque;
    Mat imageGrayScale;
    Mat imageNB;


    bool plaqueTrouve=false;
    vector<Rect> roi;
    vector<Rect> roiClone;
    Rect plate;

    if(modeImage==false)
    {

        ofstream fichier("./~communication",ios::trunc);  //déclaration du flux et ouverture du fichier

        if(fichier)  // si l'ouverture a réussi
        {
            fichier.put('0');

            fichier.close();  // on referme le fichier
        }

        cout << "avant création processus fils" << endl;
        pid_t pid = creationProcessus();
        switch (pid) {
            /* Si on a une erreur irrémédiable (ENOMEM dans notre cas) */
        case -1:
            perror("fork");
            return EXIT_FAILURE;
        break;
            /* Si on est dans le fils */
        case 0:
        {

            cout << "fils" << endl;
            char*argumentTemp[]={(char*)argumentProgramme.c_str(),NULL};
            processusFils(nomProgrammeCamera,argumentTemp);
        }
        break;
            /* Si on est dans le père */
        default:

                cout << "pere" << endl;
                while(1)
                {

                    if(modeImage==true)
                    {
                        cout << "Nom de l'image : ";
                        cin >> nomImage;
                    }
                    else
                    {
                        ifstream fichierLecture("./~communication", ios::in);
                        if (fichierLecture)
                        {
                            char caractere='0';
                            fichierLecture.get(caractere);
                            while(caractere=='0'){fichierLecture.close();sleep(1);fichierLecture.open("./~communication", ios::in);fichierLecture.get(caractere);cout << "caractereLecture : " << caractere << endl;}
                            fichierLecture.close();

                        }
                        else
                        {
                            sleep(1);
                            continue;
                        }

                    }
                    image = imread(nomImage, CV_LOAD_IMAGE_COLOR);
                    if (!image.data){cout << "Erreur lors du chargement de l'image." << endl;waitKey(100);continue;}

                    imageGrayScale = imread(nomImage, CV_LOAD_IMAGE_GRAYSCALE);

                    if (modeImage==false)
                    {
                        cout << "put(0)" << endl;
                        ofstream fichierEcriture("./~communication",ios::trunc);  //déclaration du flux et ouverture du fichier

                        if(fichierEcriture)  // si l'ouverture a réussi
                        {
                            fichierEcriture.put('0');

                            fichierEcriture.close();  // on referme le fichier
                        }
                    }

                    imshow( "window1", image );
                    waitKey(10);

                    int seuil=0;

                    int nbCaractere=4;
                    int seuilRetenu=0;
                    bool auMoinsUnePLaqueTrouve=false;
                    int aireRetenu=0;
                    Mat imageRetenu;

                    string nomFenetre;

                    for (int i =0;i<32;i++)
                    {
                        int aireTemp;
                        seuil = i*8;
                        imageNB = imageGrayScale>seuil;
                        roiClone.clear();
                        for (int a=0;a<roi.size();a++)
                        {
                            roiClone.push_back(roi[a]);
                        }
                        if (roi.size()==0)plaqueTrouve=findPlate(imageNB.clone(),plate,0);
                        else plaqueTrouve=findPlate(imageNB.clone(),plate,&roiClone,i);

                        if (plaqueTrouve){
                            if (roi.size()>0)imagePlaque=imagePlaque(roi[0]);
                            imagePlaque=image(plate);
                            imageNB=~imageNB(plate);
                            imwrite("plaque.jpg",imagePlaque);
                            nomFenetre=i+0x30;
                            int nbCaractereTemp=isolerCaractere(imageNB,aireTemp);

                            if(nbCaractereTemp>nbCaractere || (nbCaractereTemp==nbCaractere && aireTemp>aireRetenu))
                            {
                                aireRetenu=aireTemp;
                                imageRetenu=imageNB.clone();
                                auMoinsUnePLaqueTrouve=true;
                                nbCaractere=nbCaractereTemp;
                                seuilRetenu=seuil;

                            }
                        }
                    }

                    if (!auMoinsUnePLaqueTrouve) continue;

                    ocr(imageRetenu);
                    waitKey(10);
                    if (modeImage==true)waitKey(2500);
                }
        break;
        }
    }




    return 0;
}
